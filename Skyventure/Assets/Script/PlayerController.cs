﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private GameObject playerWalk;
    private CharacterController playerController;
    private GameObject playerFly;
    
    private bool isJumping = false;
    private bool isFlying = false;

    // Start is called before the first frame update
    void Start()
    {
        playerWalk = transform.Find("PlayerWalk").gameObject;
        playerController = playerWalk.GetComponent<CharacterController>();

        playerFly = transform.Find("PlayerFly").gameObject;
        playerFly.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (isJumping == false && Input.GetButtonDown("Jump"))
        {
            isJumping = true;
            return;
        }

        Debug.Log(playerController.velocity.y != 0.0f);
        if (isJumping && Input.GetButtonDown("Jump") && playerWalk.activeSelf && playerController.velocity.y != 0.0f)
        {
            playerFly.transform.position = playerWalk.transform.position;
            playerFly.transform.rotation = playerWalk.transform.rotation;
            playerFly.transform.Rotate(0, 180, 0);
            playerWalk.SetActive(false);
            playerFly.SetActive(true);
            //isJumping = false;
            isFlying = true;
        }
        else if (isFlying && Input.GetButtonDown("Jump") && playerFly.activeSelf)
        {
            playerWalk.transform.position = playerFly.transform.position;
            playerFly.SetActive(false);
            playerWalk.SetActive(true);
            playerFly.transform.rotation = Quaternion.identity;
            //isJumping = true;
            isFlying = false;
        }
    }
}
