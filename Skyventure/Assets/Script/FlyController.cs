﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyController : MonoBehaviour
{
    public float moveSpeed = 5.0f;
    public float minSpeed = 2.0f;
    public float maxSpeed = 10.0f;
    public Camera cameraFly;
    private bool isReverse = false;

    // Start is called before the first frame update
    void Start()
    {
        isReverse = (Vector3.Dot(transform.forward, Vector3.forward) < 0);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 tForward = transform.forward;
        Vector3 vForward = Vector3.forward;
        float vertical = Input.GetAxis("Vertical");
        float horizontal = Input.GetAxis("Horizontal");

        if (isReverse)
        {
            tForward = -tForward;
            vForward = -vForward;
            vertical = -vertical;
            horizontal = -horizontal;
        }

        Vector3 moveCamTo = transform.position - tForward * 10.0f + Vector3.up * 3.0f;
        float t = 0.8f;
        cameraFly.transform.position = t * cameraFly.transform.position + (1.0f - t) * moveCamTo;
        cameraFly.transform.LookAt(transform.position + tForward * 50.0f);

        moveSpeed -= tForward.y * Time.deltaTime * 30.0f;
        moveSpeed = Mathf.Clamp(moveSpeed, minSpeed, maxSpeed);

        if (Input.GetButton("Fire2"))
        {
            moveSpeed += 5.0f;
        }

        transform.Translate(vForward * moveSpeed * Time.deltaTime);
        transform.Rotate(vertical, 0, -horizontal);
    }
}
