﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssassinTree : MonoBehaviour
{
	private GameObject player;
	private ParticleSystem leaves;
	private bool collided;
	private float t;

	void Start() {
		if(player == null) {
			player = GameObject.FindWithTag("Player");
			player.transform.GetChild (3).gameObject.SetActive(false);
			//Debug.Log("player");
		} /*else {
			Debug.Log("not player");
		}*/
	}

    // Update is called once per frame
    void Update()
    {
		if(Time.time - t >= 0.6f) {
			player.transform.GetChild (3).gameObject.SetActive(false);
		}

		if(collided && Input.GetKeyDown("space")) {
			player.transform.Translate(0,0,0);
			//player.transform.GetChild (0).gameObject.SetActive(true);
			//player.transform.GetChild (1).gameObject.SetActive(true);
			player.transform.GetChild (0).gameObject
				.transform.GetChild (0).gameObject
				.transform.GetChild (0).gameObject
				.GetComponent<Renderer> ().enabled = true;
			player.transform.GetChild (1).gameObject
				.transform.GetChild (0).gameObject
				.transform.GetChild (0).gameObject
				.GetComponent<Renderer> ().enabled = true;
			collided = false;

			player.GetComponent<NewPlayerController>().walkSpeed = 5.0f;
			player.GetComponent<NewPlayerController>().gravity = -9.81f;
		}
    }

	private void OnTriggerEnter(Collider other)
    {
		if(player != null) {
        	//player.transform.GetChild (0).gameObject.SetActive(false);
			//player.transform.GetChild (1).gameObject.SetActive(false);
			player.transform.GetChild (0).gameObject
				.transform.GetChild (0).gameObject
				.transform.GetChild (0).gameObject
				.GetComponent<Renderer> ().enabled = false;
			player.transform.GetChild (1).gameObject
				.transform.GetChild (0).gameObject
				.transform.GetChild (0).gameObject
				.GetComponent<Renderer> ().enabled = false;
			collided = true;

			player.GetComponent<NewPlayerController>().walkSpeed = 0.0f;
			player.GetComponent<NewPlayerController>().flySpeed = 0.0f;
			player.GetComponent<NewPlayerController>().gravity = 0.0f;
			player.GetComponent<NewPlayerController>().playerVelocity.x = 0.0f;
			player.GetComponent<NewPlayerController>().playerVelocity.y = 0.0f;
			player.GetComponent<NewPlayerController>().playerVelocity.z = 0.0f;
			player.GetComponent<NewPlayerController>().SwitchForm();

			player.transform.GetChild (3).gameObject.SetActive(true);
			t = Time.time;

			//Debug.Log("not null");
		}/* else {
			Debug.Log("null");
		}*/
    }

	/*private void OnTriggerExit(Collider other)
    {
        transform.Translate(Vector3(0,3,0));
    }*/
}
