﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewPlayerController : MonoBehaviour
{
    // Camera parameters
    public float cameraSensitivity = 1.0f;
    public float minAngle = -35.0f;
    public float maxAngle = 60.0f;
    public float minFOV = 60.0f;
    public float maxFOV = 90.0f;
    private float mouseX, mouseY;

    // Walking parameters
    public float walkSpeed = 20.0f;
    public float jumpHeight = 1.0f;
    public float gravity = -9.81f;
    private bool groundedPlayer = false;

    // Flying parameters
    public float flySpeed = 50.0f;
    public float minSpeed = 20.0f;
    public float maxSpeed = 100.0f;
    public float boostSpeed = 150.0f;
    public float turnSpeedX = 1.0f;
    public float turnSpeedY = 1.0f;
    public float flappingCooldown = 1.0f;
    private float nextFire = 0.0f;

    // Player components
    private CharacterController controller;
    private Transform target;
    private GameObject walkForm;
    private GameObject flyForm;
    public Vector3 playerVelocity;

    // Water particule system
    private ParticleSystem waterParticules;

    void Start()
    {
        // Get controller
        controller = GetComponent<CharacterController>();

        // Hide cursor on window
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        // Get child objects
        target = transform.Find("Target");
        walkForm = transform.Find("PlayerWalk").gameObject;
        flyForm = transform.Find("PlayerFly").gameObject;
        flyForm.SetActive(false);
        waterParticules = transform.Find("WaterParticules").gameObject.GetComponent<ParticleSystem>();
        waterParticules.Stop();
    }

    void Update()
    {
        // Update movement based on mode (walk/fly)
        if (walkForm.activeSelf)
            UpdateWalk();
        else
            UpdateFly();

        // Switch between modes
        if (Input.GetButtonDown("Fire1"))
            SwitchForm();
    }

    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        // Switch to walk when touching islands
        if (flyForm.activeSelf)
            SwitchForm();
    }

    void OnTriggerEnter(Collider hit)
    {
        if (hit.tag == "WaterSurface")
            waterParticules.Play();
    }

    void OnTriggerExit(Collider hit)
    {
        if (hit.tag == "WaterSurface")
            waterParticules.Stop();
    }

    void OnTriggerStay(Collider hit)
    {
        // Accelerate when passing through wind tunnels
        if (flyForm.activeSelf && hit.gameObject.layer == 10)
            flySpeed += 1.0f;
    }

    void LateUpdate()
    {
        // Update camera movement
        mouseX += Input.GetAxis("Mouse X") * cameraSensitivity;
        mouseY += Input.GetAxis("Mouse Y") * cameraSensitivity;
        mouseY = Mathf.Clamp(mouseY, minAngle, maxAngle);

        Camera.main.transform.LookAt(target);
        target.rotation = Quaternion.Euler(mouseY, mouseX, 0.0f);

        if (flyForm.activeSelf)
        {
            float newFieldOfView = minFOV + (maxFOV - minFOV) * (flySpeed - walkSpeed) / (maxSpeed - walkSpeed);
            Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, newFieldOfView, 0.1f);
        }
        else if (Camera.main.fieldOfView != minFOV)
            Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, minFOV, 0.1f);
    }

    public void SwitchForm()
    {
        // Reset modes before switching
        if (flyForm.activeSelf)
            transform.eulerAngles = new Vector3(0.0f, transform.eulerAngles.y, 0.0f);
        else
            flySpeed = minSpeed;

        // Enable one mode and disable the other
        walkForm.SetActive(!walkForm.activeSelf);
        flyForm.SetActive(!flyForm.activeSelf);
    }

    private void UpdateWalk()
    {
        // Reset jump velocity
        groundedPlayer = controller.isGrounded;
        if (groundedPlayer && playerVelocity.y < 0.0f)
            playerVelocity.y = 0.0f;

        // Camera forward and right vectors
        Vector3 forward = Camera.main.transform.forward;
        Vector3 right = Camera.main.transform.right;

        // Apply movement
        Vector3 move = forward * Input.GetAxis("Vertical") + right * Input.GetAxis("Horizontal");
        move.y = 0.0f;
        move = move.normalized;
        controller.Move(move * walkSpeed * Time.deltaTime);

        if (move != Vector3.zero)
            transform.forward = move;

        // Make a jump and apply gravity
        if (Input.GetButtonDown("Jump") && groundedPlayer)
            playerVelocity.y += Mathf.Sqrt(-gravity * jumpHeight);

        playerVelocity.y += gravity * Time.deltaTime;
        controller.Move(playerVelocity * Time.deltaTime);
    }

    private void UpdateFly()
    {
        // Compute flight speed
        flySpeed -= transform.forward.y * Time.deltaTime * minSpeed;
        flySpeed = Mathf.Clamp(flySpeed, minSpeed, maxSpeed);

        // Apply speed when flapping wings
        if (Input.GetButtonDown("Jump") && Time.time > nextFire)
        {
            nextFire = Time.time + flappingCooldown;
            flySpeed += boostSpeed;
        }

        // Apply transformations
        controller.Move(transform.forward * flySpeed * Time.deltaTime);
        transform.Rotate(Input.GetAxis("Vertical") * turnSpeedY, 0.0f, 0.0f, Space.Self);
        transform.Rotate(0.0f, Input.GetAxis("Horizontal") * turnSpeedX, 0.0f, Space.World);
    }
}