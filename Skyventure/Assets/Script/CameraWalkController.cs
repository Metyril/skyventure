﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraWalkController : MonoBehaviour
{
    public float sensitivity;
    public Transform Target, Player;
    public float minAngle = -35.0f;
    public float maxAngle = 60.0f;
    private float mouseX, mouseY;

    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    void LateUpdate()
    {
        mouseX += Input.GetAxis("Mouse X") * sensitivity;
        mouseY -= Input.GetAxis("Mouse Y") * sensitivity;
        mouseY = Mathf.Clamp(mouseY, minAngle, maxAngle);

        transform.LookAt(Target);

        if (Input.GetButton("Fire3"))
        {
            Target.rotation = Quaternion.Euler(mouseY, mouseX, 0);
        }
        else
        {
            Target.rotation = Quaternion.Euler(mouseY, mouseX, 0);
            Player.rotation = Quaternion.Euler(0, mouseX, 0);
        }
    }
}
